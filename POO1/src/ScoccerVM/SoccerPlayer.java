package ScoccerVM;

public class SoccerPlayer extends Person {
    String position;
    byte number;

    public SoccerPlayer () {
        super();
        number = Utilities.GenerateNumber();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public byte getNumber() {
        return number;
    }

    public void setNumber(byte number) {
        this.number = number;
    }
}
